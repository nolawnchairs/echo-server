# Echo Server

Simple REST API that echos the request back

* **GET** returns the request headers, uri, and the query string as an object (if any)
* **POST** echos back the body that was sent (or an empty object) with a status of 201
* **PUT** and **PATCH** echos back the body that was sent if any with a status 200
* **DELETE** returns an empty response with a staus of 204

> All request bodies can either be JSON or Form-encoded

Useful as a placeholder application
