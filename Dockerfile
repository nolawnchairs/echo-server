FROM node:14.16.1-alpine3.13

RUN mkdir /app && touch /app/.env
RUN chown -R node:node /app

USER node

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 80

ENV PORT 80

CMD ["npm", "start"]