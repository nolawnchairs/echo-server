
require('dotenv').config()

const express = require('express')
const app = express()
const PORT = process.env.PORT || 8008

function postResponse(status) {
  return (req, res) => {
    const { body } = req
    res.status(status).json(body)
  }
}

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.get('*', (req, res) => {
  const { query, headers, url } = req
  res.json({ url, headers, query })
})

app.post('*', postResponse(201))
app.put('*', postResponse(200))
app.patch('*', postResponse(200))
app.delete('*', (_, res) => res.status(204).send())

app.listen(PORT, () => {
  console.log(`Echo server running on port ${PORT}`)
})
